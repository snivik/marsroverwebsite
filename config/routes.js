/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add.ejs an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

/* Session */

   'get /auth/callback/:strategy' : 'SessionController.callback',
   'get /auth/login/:strategy' : 'SessionController.login',
   'get /auth/logout' : 'SessionController.logout',

/* User */

  'get /users/manage' : 'UserController.manage',
  'get /user/edit/:id': 'UserController.edit',
  'get /user/:id' : 'UserController.view',
  'post /user/:id': 'UserController.update',




/* Articles */


  'get /article/add' : 'ArticleController.add',
  'get /articles/manage' :'ArticleController.manage',


  'get /article/:id' : 'ArticleController.edit',
  'get /articles/:page': 'ArticleController.index',

  // Stuff to up
  'post /article' : 'ArticleController.create',
  'post /article/:id' : 'ArticleController.update',
  'delete /article/:id' : 'ArticleController.delete',

/* Sponsor */

  // GET
  'get /sponsor/add' : 'SponsorController.add',
  'get /sponsor/manage' : 'SponsorController.create',
  'get /sponsor/:id' : 'SponsorController.view',


  'get /sponsors' : 'SponsorController.index',

  // POST
  'post /sponsor/:id' : 'SponsorController.update',
  'post /sponsor' : 'SponsorController.create',
  'delete /sponsor' : 'SponsorController.delete',


  /* About */
  'get /about/assistance': { view: 'about/assistance'},
  'get /about/traversal': { view: 'about/traversal'},
  'get /about/science' : {view: 'about/science'},
  'get /about/servicing' : {view: 'about/servicing'},

  'get /about/team':'AboutController.team',
  'get /about/cs': 'About.computerscience',


  /* Meetings */
  'get /meeting/add': 'MeetingController.add',
  'get /meetings/view': 'MeetingController.view',
  'get /meetings/manage': 'MeetingController.manage',

   // Page routing
  'get /' : 'ArticleController.index'


};
