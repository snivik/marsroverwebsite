/**
 * Created by Troshchenko on 1/23/15.
 */


var db = connect("127.0.0.1:27017/MarsRover");



var groups = ['Mercury', 'Gemini', 'Apollo', 'Skylab', 'Space Shuttle', 'Space Station']



for (var i=0;i<groups.length;i++){

  // Get a group
  var group = groups[i];

  // Generate 10 users per group
  for (var j=0;j<5;j++){

    db.sponsor.insert(
      {
        owner_id: 'SYSTEM',
        sponsorship_level: groups[i],
        contact_info: 'it@it.edu',
        name: 'Test Company',
        about: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
        company_picture_url: '/img/icons/sample_company.png',
        donation_description: '$1000',
        show_contact_info: i % 3 == 2,
        show_donation_description: i % 2 == 1,
        verified: true

      })

  }


}

