/**
 * Created by mains_000 on 1/5/2015.
 */
module.exports = function(req, res, next) {

  // User is allowed, proceed to the next policy,
  // or if this is the last policy, the controller
  if (req.session.team_member || req.session.admin) {


    return next();

  } else {

    // User is not allowed
    // (default res.forbidden() behavior can be overridden in `config/403.js`)
    req.session.flash = 'You must be a Team Member to access this page';
    return res.forbidden('You have to be Team Leader to perform this action');

  }

};
