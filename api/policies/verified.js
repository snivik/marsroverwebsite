/**
 * Created by mains_000 on 1/5/2015.
 */
module.exports = function(req, res, next) {

  // User is allowed, proceed to the next policy,
  // or if this is the last policy, the controller
  if (req.session.user.verified) {
    return next();
  }

  // User is not allowed
  // (default res.forbidden() behavior can be overridden in `config/403.js`)
  req.session.flash = 'You must be verified prior accessing this page';
  return res.forbidden('You have to be be verified first.');
};
