/**
 * Created by mains_000 on 1/5/2015.
 */
module.exports = function(req, res, next) {

  // User is allowed, proceed to the next policy,
  // or if this is the last policy, the controller
  if (req.session.auth.team_leader || req.session.admin) {
    return next();
  }

  // User is not allowed
  // (default res.forbidden() behavior can be overridden in `config/403.js`)
  req.session.flash = 'You must be a Team Leader';
  return res.forbidden('You have to be Team Leader to perform this action');
};
