/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/



module.exports = {

  attributes: {

    // Different IDs that user has
    facebook_id: {type: 'string', unique: true},
    google_id : {type: 'string', unique: true},


    // Personal Data
    last_name: {type: 'string', required: true},
    first_name: {type: 'string', required: true},

    avatar_url: {
      type: 'string',
      defaultsTo: "/img/icons/default_user.png"
    },

    about : {
      type: 'string',
      defaultsTo: 'No description available'
    },


    // About
    subgroup: {type: 'string', required: true, defaultsTo: 'Not specified'},
    major : {type: 'string', defaultsTo: 'Not specified'},
    email : {type: 'email', required: true, unique: true},


    // Access Levels
    team_member: {
      type: 'boolean',
      defaultsTo: false
    },

    admin: {
      type: 'boolean',
      defaultsTo: false
    },


    team_leader: {
      type: 'boolean',
      defaultsTo: false
    },


    verified: {
      type: 'boolean',
      defaultsTo: false
    },


    blacklisted: {
      type: 'boolean',
      defaultsTo: false
    }


  }





};

