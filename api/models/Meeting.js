/**
* Meeting.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
      owner_id: {type: 'string'},
      student_name: {type: 'string', required: true, defaultsTo: 'Noone'},

      date : {type: 'date', required: true},
      start_time: {type: 'date', required: true},
      end_time: {type: 'date', required: false},
      description: {type: 'string', required: false},

      verified: {type: 'boolean', defaultsTo: true}
  }
};

