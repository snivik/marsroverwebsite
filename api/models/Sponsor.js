/**
* Sponsor.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    // Creator
    owner_id: {type: 'string', required: true},

    // Sponsorship Level
    sponsorship_level: {type: 'string'},

    // Contact Information
    contact_info : {type: 'string', defaultsTo: 'No contact information provided'},

    // Public info
    name: {type: 'string', defaultsTo: 'Anonymous'},
    about : {type: 'string', defaultsTo: 'No description provided'},
    company_picture_url : {type: 'string', defaultsTo: '/img/icons/sample_company.png'},

    // Donation type
    donation_description: {type: 'string', defaultsTo: 'No donation'},

    // Display Settings
    show_contact_info : {type: 'boolean', defaultsTo: false},
    show_donation_description : {type: 'boolean', defaultsTo: false},

    verified: {type: 'boolean', defaultsTo: false}


  }
};

