/**
 * ArticleController
 *
 * @description :: Server-side logic for managing Articles
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var articles_per_page = 5;


module.exports = {

  // GET Requests
  add: function(req,res){
    res.view();
  },

  index: function(req,res) {

    // Get the page number
    var page = req.param('page');

    // Deafult the page
    if (!page) {
      page = 0
    }

    // Get all the stuff.
    Article.find({verified: true}).sort("date_added DESC").exec(function (err, articles) {

      // Error handling
      if (err) {
        res.redirect('/');
        req.session.flash = 'Error while trying to retrieve articles from the database';
      }

      // Selectors
      var index = page * articles_per_page;

      // Define whether to put "Go back and Go forward"
      var hasOlder = true;
      var hasNewer = page > 0;

      var articleSubset = new Array();

      // Insert articles in returnableArticles
      while (articleSubset.length < articles_per_page && index < articles.length) {
        articleSubset.push(articles[index]);
        index++;

      }

      // Now identify if we have older articles
      // If we are at the end or we have received less than 5 articles
      if (index == articles.length || articleSubset.length < articles_per_page) {
        hasOlder = false;
      }

      return res.view({

        hasOlder: hasOlder,
        hasNewer: hasNewer,
        articles: articleSubset,
        page: page

      })

    })

  },

  edit: function(req, res){

    var id = req.param('id');

    // Check the existence of the ID
    if (!id){
      req.session.flash = 'No article ID provided, do not try to access them manually';
      return res.redirect('/')
    }


    // Find the article
    Article.findOne({id: id}, function(err, article){

      // If error while finding, log it and return to main page
      if (err){
        console.log(err);
        req.session.flash = "Failed to load this article for editing, please try again later"
        return res.redirect('/')
      } else if (!article){

        req.session.flash = "Article with that ID was not found. Perhaps it was deleted?"
        return res.redirect('/')

      } else {

        return res.view({article: article});

      }

    });

  },

  /**
   * Manage view loads articles that are not verified
   * @param req
   * @param res
   */
  manage: function(req,res){

    Article.find({verified: false}, function(err, articles){

      if (err){

        req.session.flash = 'Failed to fetch unverified articles'
        res.redirect('/')

      } else if (!articles){

        req.session.flash = 'All articles are approved';
        res.redirect('/');

      } else {

        res.view({articles: articles});

      }

    });

  },


  // POST Requests
  create: function(req, res){

    var article = {};

    // Prefill the article
    article.title = req.param('title');
    article.text = req.param('text');
    article.author = req.session.user.id;
    article.verified = req.session.admin;
    article.date_added = new Date();

    // Add to the database
    Article.create(article, function(err, article){

      if (err){

        log.error(err);
        req.session.flash = "Failed to add an article, please try again later"
        return res.json({success: false, message: 'Failed to add an article due to internal error'})

        //
      } else {
        req.session.flash = 'Post submitted, please await the approval'
        return res.json({success: true, redirectTo: '/'})
      }

    })



  },

  update: function(req,res){

    // Make sure the request is there.
    var id = req.param('id');

    var article = {}

    // That's everything that can be updated in the article automatically

    if (req.param('title')) {
      article.title = req.param('title');
    }

    if (req.param('text')) {
      article.text = req.param('text');
    }

    article.verified = (req.param('verified')&& req.session.admin) ? true : false;


    if (!id){
      return res.json({success: false, message: 'article ID is not provided, are you sure you are editing the correct item?'})
    }

    // Find the article & update it
    Article.update({id: id}, article, function(err, articles){

      if (err){
        console.log(err)
        return res.json({success: false, message:  'Can\'t update article, please try again later.'});
      } else if (articles.length == 0){
        return res.json({success: false, message:  'No articles were found under this ID'});
      } else {
        req.session.flash = 'Article Updated.';
        return res.json({success: true, reload: true });
      }

    })

  },

  delete: function(req, res){

    var id = req.param('id')

    if (!id){
      return res.json({success: false, message: 'Id not specified'})
    }

    Article.destroy({id: id}).exec( function(err, articles){

      if (err){
        console.log(err)
        return res.json({success: false, message: 'Error while deleting article, please try again later'})
      }

      return res.json({success: true, reload: true})

    })


  }

};

