/**
 * AboutController
 *
 * @description :: Server-side logic for managing Abouts
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {


  team: function(req,res){

    User.find({team_member: true, verified: true}, function(err, users){

        if (err) {

          console.log(err);
          req.session.flash('Failed to fetch users');


        } else {

          // Make groups for all the subgroups
          var uavGroup = new Array(), roverGroup = new Array(), controlsGroup = new Array(), armGroup = new Array(), teamleads = new Array();

          // Scan all the users
          for (var i=0;i<users.length;i++){


            // Put in the certain array.
            switch (users[i].subgroup){

              case 'UAV':
                uavGroup.push(users[i]);
                break;
              case 'Controls':
                controlsGroup.push(users[i]);
                break;
              case 'Arm':
                armGroup.push(users[i]);
                break;
              case 'Rover':
                roverGroup.push(users[i]);
                break;
              case 'Team Leader':
                teamleads.push(users[i]);
                break;

            }

          }


          // View
          return res.view({
            sections:
            [
              {
                id: 'rover',
                bg: '/img/pattern/pattern_rover.png',
                members: roverGroup,
                title: 'ROVER',
                description: 'The Rover subgroup is designing and implementing rover mechanics. This group is focusing on the body design, motors and ' +
                'other physical properties of the system as well as making sure its ability to complete various tasks.',
                icon: '/img/icons/rover.png'
              },
              {
                id: 'uav',
                bg: '/img/pattern/pattern_uav.png',
                members: uavGroup,
                title: 'UNMANNED AERIAL VEHICLE',
                description: 'UAV subgroup is designing and implementing the whole UAV system, starting from the drone itself, finishing with the controls. The UAV will ' +
                'assist rover in various tasks throughout the competition and will ensure excellent performance in the time sensitive tasks.',
                icon: '/img/icons/satellite.png'
              },
              {
                id: 'arm',
                bg: '/img/pattern/pattern_arm.png',
                members: armGroup,
                title: 'ROBOTIC ARM',
                description: 'We have a separate group of engineers that are implementing a robotic arm. The robotic arm will be one of the most important parts in tasks like \'Astronaut assistance\' or \'Equipment Servicing\'. ' +
                'Robotic arm is required to have great precision to speed up the task completion and we will make sure it does.',
                icon: '/img/icons/arm.png'
              },
              {
                id: 'controls',
                bg: '/img/pattern/pattern_controls.png',
                members: controlsGroup,
                title: 'CONTROLS',
                description: 'In the controls subgroup we are making sure that we can provide intuitive and comfortable operation of the rover and arm. In this competition, performance is a key feature, ' +
                'and we will make sure that controlling the rover will be as easy as walking. ',
                icon: '/img/icons/basestation.png'
              },
              {
                id: 'teamleads',
                bg: '/img/pattern/pattern_leads.png',
                members: teamleads,
                title: 'TEAM LEADERS',
                description: 'With the amount of subgroups in our team, making sure all of them are synchronized and are able to work together.',
                icon: '/img/icons/manager.png'
              },

            ]
          })

        }




    });

  },

  computerscience: function(req, res){

    User.find({verified: true, team_member: true}, function (err, users){

      if (!err){

        var csStudents = new Array();
        for (var i=0;i<users.length;i++){

          if (users[i].major == 'Software Engineering' || users[i].major == 'Computer Science'){
            csStudents.push(users[i])
          }
        }

        return res.view({users: csStudents});
      }

    });

  }
};

