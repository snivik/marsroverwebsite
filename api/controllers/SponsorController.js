/**
 * SponsorController
 *
 * @description :: Server-side logic for managing Sponsors
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */


/**
 * Assembles basic information about the sponsor from the request. Doesn't
 * Include information about verified sponsor
 * @param req
 */
function assembleSponsor(req){

  var spons = {};
  spons.contact_info = req.param('contact_info');
  spons.name = req.param('name');
  spons.about = req.param('about');

  if (req.param('company_picture_url') && req.param('company_picture_url') != ''){
    spons.company_picture_url = req.param('company_picture_url');
  }
  if (req.param('donation_description') && req.param('donation_description') != ''){
    spons.donation_description = req.param('donation_description');
  }
  spons.show_contact_info = req.param('show_contact_info') ? true : false;
  spons.show_donation_description = req.param('show_donation_description') ? true : false;


  return spons;
}


module.exports = {

  add: function(req, res){

    Sponsor.findOne({owner_id: req.session.user.id}, function(err, sponsor){

      // Error handling
      if (err){
        console.log(err);
        req.session.flash = 'This page is not available';
        res.redirect('/');
      // Existing Sponsor handling
      } else if (sponsor && !req.session.admin && !req.session.team_leader){
        req.session.flash = 'You have already added a sponsor.';
        res.redirect('/sponsor/'+sponsor.id);

        // Everything is good, redirect to the view
      } else {
        return res.view();
      }

    })

  },


  /**
   * `SponsorController.index()`
   */
  index: function (req, res) {

    Sponsor.find({verified: true}, function(err, sponsors){

      if (err){
        console.log(err)
        req.session.flash = 'Failed to load'
        res.redirect('/')

      } else {

        if (sponsors.length == 0){
          req.session.flash = 'There are no sponsors at the moment, maybe we will have some in the future...';
          res.redirect('/')
        } else {

          var mercury={}, gemini={}, apollo={}, skylab={}, space_shuttle={}, space_station={};

          var groups = [space_station, space_shuttle, skylab, apollo, gemini, mercury];

          for (var i=0;i<groups.length;i++){

            groups[i].sponsors = new Array();
          }

          for (var i=0;i<sponsors.length;i++){

            sponsorship_level = 'mercury'

            if (sponsors[i].sponsorship_level){
              sponsorship_level=sponsors[i].sponsorship_level
            }

            switch (sponsorship_level){

              case 'mercury':
                    mercury.sponsors.push(sponsors[i])
                    break;
              case 'gemini':
                    gemini.sponsors.push(sponsors[i])
                    break;
              case 'apollo':
                    apollo.sponsors.push(sponsors[i])
                    break;
              case 'skylab':
                    skylab.sponsors.push(sponsors[i])
                    break;
              case 'space shuttle':
                    space_shuttle.sponsors.push(sponsors[i])
                    break;
              case 'space station':
                    space_station.sponsors.push(sponsors[i])
                    break;
            }
          }

          return res.view({
            sponsors: groups
          })

        }



      }

    })

  },


  /**
   * `SponsorController.manage()`
   */
  manage: function (req, res) {
    return res.json({
      todo: 'manage() is not implemented yet!'
    });
  },


  /**
   * `SponsorController.view()`
   */
  view: function (req, res) {

    Sponsor.findOne({id: req.param('id')}, function(err, sponsor){

      if (err){
        log.error(err)
        req.session.flash = 'Can\'t load a sponsor due to internal issue, please try again later';
        return res.redirect('/');
      } else if (sponsor){

        if (sponsor.verified){
          return res.view({sponsor: sponsor});
        } else if (sponsor.owner_id == req.session.owner_id || req.session.admin || req.session.team_leader){
          return res.view({sponsor: sponsor});
        } else {

          req.session.flash = 'Sponsor not approved yet'
          return res.redirect('/')

        }

      }

    })

  },



  /*  POST METHODS    */

  /**
   * `SponsorController.create()`
   */
  create: function (req, res) {

    Sponsor.findOne({owner_id: req.session.user.id}, function(err, sponsor){

      // Error handling
      if (err){
        console.log(err);
        return res.json({success: false, message: 'Failed to ge existing sponsors from the database'});
        // Existing Sponsor handling
      } else if (sponsor && !req.session.admin && !req.session.team_leader){
        req.session.flash = 'You have already added a sponsor';
        return res.json({success: true, redirectTo: '/sponsor/'+sponsor.id });
        // Everything is good, redirect to the view
      } else {

        var spons = assembleSponsor(req);

        spons.owner_id = req.session.user.id;
        Sponsor.create(spons, function(err, sponsor){

          if (err){

            console.log(err)
            return res.json({success: false, message: 'Internal Error while attempting to register sponsor. Please try again later'});
          }
          else {
            req.session.flash = 'Sponsorship request sent';
            return res.json({success: true, redirectTo: '/'})
          }


        })



    }})

  },


  /**
   * `SponsorController.update()`
   */
  update: function (req, res) {
    var sponsor = assembleSponsor(req);

    // Add verified if it's admin
    var search_query = {id: req.param('id')};

    if (!req.session.admin){
      search_query.owner_id = req.session.user.id;
    }

    if (req.session.admin || req.session.team_leader){
      sponsor.verified = req.param('verified')
    }

    Sponsor.update(search_query, sponsor, function(err, sponsors){

      if (err){
        console.log(err)
        return res.json({success: false, message: 'Internal Error while updating a sponsor'})
      } else if (sponsors.length == 0){

        return res.json({success: false, message: 'Sponsor not found or you are updating someone else\'s Sponsor'})
      } else {
        req.session.flash = 'Sponsorship was submitted for approval'
        return res.json({success: true, redirectTo: '/'});
      }

    });
  },


  /**
   * `SponsorController.delete()`
   */
  delete: function (req, res) {

    Sponsor.delete({id: req.param('id')}, function(err, deletedSponsors){
      if (err){
        console.log(err)
        return res.json({success: false, message: 'Internal error, try again later'});
      } else if (deletedSponsors.length == 0) {
        return res.json({success: false, message: 'Didn\'t find sponsors with this ID'});
      } else {
        return res.json({success: true, reload: true});
      }
    })

  }
};

