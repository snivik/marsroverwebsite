/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */





module.exports = {


  view : function (req, res) {

    var id = req.param('id');
    if (!id){
      req.session.flash = "User with this ID doesn't exist"
      return res.redirect('/')
    }

    User.findOne({id: id}, function(err, user){

      if (err){
        console.log(err)
        req.session.flash = 'Failed to get a user, please try again later'
        return res.redirect('/')
      } else if (!user) {
        req.session.flash = 'User not found'
        return res.redirect('/')
      } else {

        return res.view({
          query_user: user
        })
      }

    });


  },


  edit : function (req, res) {

    var id = req.param('id');
    if (!id){
      req.session.flash = "User with this ID doesn't exist"
      return res.redirect('/')
    }

    User.findOne({id: id}, function(err, user){

      if (err){
        console.log(err)
        req.session.flash = 'Failed to get a user, please try again later'
        return res.redirect('/')
      } else if (!user) {
        req.session.flash = 'User not found'
        return res.redirect('/')
      } else {

        return res.view({
          query_user: user
        })
      }

    });


  },


  manage: function(req,res){

    // Find all users
    User.find({}, function(err,users){

      // Handle Errors
      if (err){
        console.log(err);
        req.session.flash('Internal Error')
        res.redirect('/');

      } else {

        var pending_users = new Array(), verified_users = new Array();

        // Sort users into groups
        for (var i=0;i<users.length;i++){

          // Push verified or banned users to one list
          if (users[i].verified || users[i].blacklisted){
            verified_users.push(users[i])

          // And pending one to another
          } else {
            pending_users.push(users[i]);
          }

        }

        // Return the appropriate view
        return res.view({
          pending_users: pending_users,
          verified_users: verified_users
        })
      }

    })
  },


  /*** POST ***/

  /**
   * `UserController.update()`
   */
  update: function (req, res) {

    // Make sure it's either owner or admin
    if (!(req.session.user.id == req.param('id') || req.session.admin)){
      return res.json({success: false, message: 'You don\'t have enough permissions to edit someone else\'s account'});
    }

    // Create user object to update
    var user = {}

    // Add some admin settings
    if (req.session.admin){
      user.team_leader = req.param('team_leader') ? true : false
      user.team_member = req.param('team_member') ? true : false
      user.verified = req.param('verified') ? true : false
      user.blacklisted = req.param('blacklisted') ? true : false
    }

    // Now settings that can be changed only by user

    user.about = req.param('about')
    user.subgroup = req.param('subgroup')
    user.major = req.param('major')

    // Now update it
    User.update({id: req.param('id')}, user, function(err, updated){

      if (err){
        console.log(err)
        return res.json({success: false, message: 'Failed to update user due to internal error. Please try again later'})
      } else if (updated.length == 0){
        return res.json({success: false, message: 'Failed to find a user with this ID'})
      } else {
        req.session.flash = 'Profile updated!'
        return res.json({success: true, reload: true})
      }



    })



  }

};

