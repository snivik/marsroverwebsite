/**
 * Session Controller
 *
 * @description :: Server-side logic for managing Auths
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var passport = require('passport');
var available_strategies = [

  {
    name: 'facebook',
    scope: ['email', 'user_about_me']
  },

  {
    name: 'google',
    scope: ['https://www.googleapis.com/auth/userinfo.profile', 'https://www.googleapis.com/auth/userinfo.email']
  }
];

/**
 * Function to check if there is item in array or not
 * @param array
 * @param obj
 * @returns {boolean}
 */
function contains(array, obj) {
  for (var i = 0; i < array.length; i++) {
    if (array[i].name === obj) {
      return array[i];
    }
  }
  return false;
}


module.exports = {


  // Login with an external command
  login: function (req, res, next){

    var strategy = contains(available_strategies, req.param('strategy'));
    if (strategy) {
      sails.log.verbose('External login requested with: ' + req.param('strategy'))

      passport.authenticate(strategy.name, {scope: strategy.scope},
        function (err, user) {
          if (!err) {

            console.log('Callback received in sender.')
            return res.redirect('/');
          } else {
            console.log('Error while sending request: ' + err)

            req.session.flash = "Couldn't authenticate user, please try again later."
            return res.redirect('/')
          }
        })(req, res, next);
    } else {

      req.session.flash = "The authorization method '"+req.param('strategy')+"' is not available at the moment"
      res.redirect("/404")
    }

  },


  callback: function(req, res, next){
    var strategy = contains(available_strategies, req.param('strategy'));
    if (strategy){

      passport.authenticate( strategy.name, { scope: strategy.scope},
        function (err, user) {
          if (!err){



              req.session.auth = true;
              req.session.admin = user.admin;
              req.session.verified = user.verified;
              req.session.team_member = user.team_member;
              req.session.team_leader = user.team_leader;
              req.session.user = user;




            return res.redirect('/');
          } else {

            sails.log('Error while reading feedback: '+err);

            req.session.flash = "Could not authenticate user due to incorrect feedback from authenticating service. Please try again later"
            return res.redirect('/')

          }
        })(req, res, next);


    } else {

      res.redirect("/404")

    }





  },


  logout: function(req, res){


    // Default the session.
    req.session.auth = false;
    req.session.user = null;
    req.session.admin =false;
    req.session.team_leader = false;
    req.session.team_member = false;
    req.session.team_member = false;

    // Flash the image
    req.session.flash = "You have successfully logged out"

    // Usual redirect
    return res.redirect('/');

  }

};

