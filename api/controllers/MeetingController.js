/**
 * MeetingController
 *
 * @description :: Server-side logic for managing Meetings
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

/**
 * Function assembles meeting out of provided data in the request
 * @param req
 */
function assembleMeeting(req){

  var meeting = {}
  meeting.student_name = req.session.user.last_name+' '+req.session.user.first_name;
  meeting.date = req.param('date');
  meeting.start_time = req.param('start_time')
  meeting.end_time = req.param('end_time')
  meeting.description = req.param('description')

  // If it's admin or teamlead, add ability approve timesheets.
  if (req.session.team_leader || req.session.admin){
    meeting.verified = req.param('verified') ? '' : undefined;
  }

}

module.exports = {



  /* VIEWS */

  // Create new meeting
  add: function (req, res){
    return res.view({})
  },

  // Manage unverified meeting minutes
  manage: function(req, res){

  },

  /* View all the meetings that belong to the user */
  view: function(req, res){
    Meeting.find({owner_id: req.session.user.id}, function (err, meetings){

      // Error handling
      if (err){
        console.log(err)
        req.session.flash = 'Something went wrong, please try again later'
        res.redirect('/')

      } else {
        res.view({meetings: meetings});
      }

    })
  },



  /* POST */
  create: function (req,res){
    var meeting = assembleMeeting(req);
    meeting.owner_id = req.session.user.id;

    // Try to add this to the database
    Meeting.create(meeting, function(err){

      if (err){
        console.log(err)
        return res.json({success: false, message: 'Failed to create a report, are you sured you filled everything out? If yes, it is our problem'});

      } else {

        return res.json({success: true, reload: true})
      }
    })
  }


};

