/**
 * Created by Alexander Troshchenko
 * @type {exports}
 */

var passport = require('passport');
var FacebookStrategy = require('passport-facebook').Strategy;


/**
 * Function that tries to create or update user based on success or failure
 * @param queries
 * @param values
 * @param done
 */
function updateOrCreateUser(queries, values, done){

  // It's time to create user
  if (queries.length == 0){

    sails.log.verbose("Creating new user")
    User.create(values, function(err, user){

      if (err){
        sails.log("Failed to create a user")
        done(err, null);
      } else {

        sails.log.verbose("User created")
        done(null, user)
      }

    })

    // Run another search query
  } else {

    var query = queries.pop();
    sails.log.verbose("Trying query:")
    sails.log.verbose(query)
    User.findOne(query, function(err, db_profile){

      // Check for error
      if (err){
        sails.log("Error while searching user using query")
        done(err, null);
      } else if (!db_profile){
        sails.log.verbose("User not found using query, trying next one")
        // Recursive call
        updateOrCreateUser(queries, values, done)
      } else {

        sails.log.verbose("User found, updating")
        // Found it, now update
        User.update(query, values, function(err, users){

          if (err){
            sails.log("Failed to update user")
            done(err, null)
          } else {
            sails.log.verbose("User updated")

            done(null, users[0]);
          }

        })

      }



    });
  }

}



///////////////////////////////////////////////

///         AUTH     STRATEGIES        ////////

///////////////////////////////////////////////



// Facebook Authorization
passport.use(new FacebookStrategy({

    // Test
    clientID: '935106049848067',
    clientSecret: "2c4894eb1753f83bcaf4d1d45e889f44",
    callbackURL: "http://localhost:1337/auth/callback/facebook"

    // Test 1337 on MarsRover
    //clientID: '947788221913183',
    //clientSecret: "5ffc78563d1043ed15f4cdcee8eec3cd",
    //callbackURL: "http://floridatechmarsrover.com:1337/auth/callback/facebook"

    // Production
    //clientID: '933174603374545',
    //clientSecret: "a1a84ccc96027793609b588cf7eb3473",
    //callbackURL: "http://floridatechmarsrover.com/auth/callback/facebook"
  },

  function(accessToken, refreshToken, profile, done) {

    var db_profile = {};

    db_profile.facebook_id = profile.id;
    db_profile.last_name = profile._json.last_name;
    db_profile.first_name = profile._json.first_name;
    db_profile.avatar_url = "http://graph.facebook.com/"+profile.id+"/picture?type=large"
    db_profile.email = profile._json.email;

    // Now queries
    var db_queries = Array();
    db_queries.push({email: db_profile.email})
    db_queries.push({facebook_id: db_profile.facebook_id});

    updateOrCreateUser(db_queries, db_profile, done);

  }
));


// Google authorization


var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;

passport.use(new GoogleStrategy({
    clientID: '609920731078-soe1laf7ob9525jj4m8irnq7abj4tu97.apps.googleusercontent.com',
    clientSecret: 'nvWgJvFnfLZDiJXHNlTJ8MDi',
    callbackURL: "http://localhost:1337/auth/callback/google"
    // callbackURL: "http://floridatechmarsrover.com:1337/auth/callback/google"
    // callbackURL: "http://floridatechmarsrover.com/auth/callback/google"
  },
  function(accessToken, refreshToken, profile, done) {

    var db_profile = {};

    db_profile.google_id = profile.id;
    db_profile.last_name = profile._json.family_name;
    db_profile.first_name = profile._json.given_name;
    db_profile.avatar_url = profile._json.picture;
    db_profile.email = profile._json.email;

    // Now queries
    var db_queries = Array();
    db_queries.push({email: db_profile.email})
    db_queries.push({google_id: db_profile.google_id});

    updateOrCreateUser(db_queries, db_profile, done);

  }
));
