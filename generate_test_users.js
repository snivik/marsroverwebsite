/**
 * Created by Troshchenko on 1/23/15.
 */


var db = connect("127.0.0.1:27017/MarsRover");



var groups = ['UAV', 'Rover', 'Team Leader', 'Arm', 'Controls']



for (var i=0;i<groups.length;i++){

  // Get a group
  var group = groups[i];

  // Generate 10 users per group
  for (var j=0;j<5;j++){

    db.user.insert(
      {
        email: 'dummy_user'+group+'_'+j+'@gmail.com',
        first_name: 'John',
        last_name: 'AppleSeed',
        avatar_url: '/img/icons/default_user.png',
        about: 'I am a dummy user and have no profile',
        subgroup: group,
        major: 'Mechanical Engineering',
        team_member: true,
        verified: true
      })

  }


}

