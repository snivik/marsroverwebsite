/**
 * Created by Troshchenko on 12/7/14.
 */



// Form override
$('form').on('submit', function(args){

  var form = $(args.target);

  console.dir(form.serializeArray());


  var jqxhr = $.ajax({

    url: form.prop('action'),
    data: form.serializeArray(),
    type: form.prop('method')
  }).done(function(data,textStatus, jqResponse){

    defaultRefreshHandler(data)

  }).fail(function(data, textStatus, jqResponse){

    alert(textStatus);

  });

  return false;
});


/**
 * Default response handler with refresh
 * @param response
 */
function defaultRefreshHandler(response){

  if (response.success == true){

    if (response.reload == true){
      document.location.reload();
    } else if (response.redirectTo) {
      document.location = response.redirectTo
    }

  } else {
    alert(response.message);
  }

}
